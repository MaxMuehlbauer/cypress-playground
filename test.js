const cypress = require("cypress");

cypress.run({
  browser: "chrome",
  headless: true
}).then((result) => {
  if (result.totalFailed > 0) {
    console.error("Tests failed");
    process.exit(1);
  } else {
    process.exit(0);
  }
}).catch((e) => {
  console.error(e);
  process.exit(1);
});
