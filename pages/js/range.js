const RANGE_MODE = {
  VALUE: "value",
  LOADING: "loading"
};

class Range {
  constructor(elem, options) {
    this.min = options.min;
    this.max = options.max;
    this.elem = elem;
    this.value = undefined;
    this.mode = RANGE_MODE.LOADING;
  }

  set value(value) {
    if (typeof value === "number" && value >= this.min && value <= this.max) {
      this.mode = RANGE_MODE.VALUE;
      this.elem.dataset.value = value;
      this.elem.style.setProperty("--value", `${value}`);
      this._value = value;
    } else {
      this.mode = RANGE_MODE.LOADING;
      delete this.elem.dataset.value;
      this.elem.style.removeProperty("--value");
      this._value = undefined;
    }
  }

  get value() {
    return this._value;
  }

  set mode(mode) {
    this._mode = mode;
    this.elem.dataset.mode = mode;
  }

  get mode() {
    return this._mode;
  }
}
