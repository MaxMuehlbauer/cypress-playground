describe("Test 01", function () {
  beforeEach(function () {
    cy.visit("/pages/test-01.html");
  });

  it("successfully sets initial mode", function () {
    cy.get(".range")
        .then(($range) => {
          expect($range).to.have.data("mode", "loading");
        })
  });

  it("successfully sets mode when valid value is set", function () {
    cy.get("#range")
        .type("50{enter}");

    cy.get(".range")
        .then(($range) => {
          expect($range).to.have.data("mode", "value");
        })
  });

  it("successfully changes value set as css variable", function () {
    cy.get("#range")
        .type("75{enter}");

    cy.get(".range")
        .then(($range) => {
          expect($range[0].style.getPropertyValue("--value")).to.equal("75");
        })
  });

  it("successfully sets value on button click", function () {
    cy.get("#range")
        .type("25");

    cy.get("[type='submit']")
        .click();

    cy.get(".range")
        .then(($range) => {
          expect($range).to.have.data("mode", "value");
          expect($range[0].style.getPropertyValue("--value")).to.equal("25");
        })
  });

  it("successfully resets everything on button click", function () {
    cy.get("[type='reset']")
        .click();

    cy.get(".range")
        .then(($range) => {
          expect($range).to.have.data("mode", "loading");
        });
  });

  it("is accessible", function () {
    cy.injectAxe();
    cy.checkA11y("main");
  });

  it("looks as expected in value mode", function () {
    cy.get("#range")
        .type("75{enter}")
        .then(() => {
          cy.get("main")
              .toMatchImageSnapshot();
        })
  })
});
