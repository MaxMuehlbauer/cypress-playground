# cypress-playground
| Feature | Status | Comment |
| ------- | ------ | ------- |
| E2E Testing | ✔ | |
| A11y Testing | ✔ | [cypress-axe](https://github.com/avanslaars/cypress-axe)|
| Unit Testing | ❌ | Not actually a key aspect of cypress, but easily possible |
| Code Coverage | ❌ | Only relevant for Unit Testing; tricky to combine Unit and E2E Testing |
| Visual Regression Testing | ✔ | [cypress-plugin-snapshots](https://github.com/meinaart/cypress-plugin-snapshots); Currently unable to run with all tests in GUI mode |
| Cross-Browser-Testing | ❔ | |
| Viewport Testing | ❔ | |
| HTML Validation | ❔ | |
